<?php

namespace Database\Seeders;

use App\Definitions\RoomDefinitions;
use App\Models\Room\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(!Room::query()->exists())
            foreach (RoomDefinitions::ROOMS as $room)
            {
                Room::query()->create($room);
            }




    }
}
