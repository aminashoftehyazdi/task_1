<?php

namespace Database\Seeders;

use App\Definitions\TimeSlotDefinitions;
use App\Models\TimeSlot\TimeSlot;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TimeSlotSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!TimeSlot::query()->exists()) {
            $today = Carbon::now()->startOfDay()->addHours(TimeSlotDefinitions::DAY_START_AT);
            for ($d = 0; $d < TimeSlotDefinitions::CYCLE_DAYS; $d++) {
                $day = $today->addDays($d);
                for ($s = 0; $s < TimeSlotDefinitions::SLOT_COUNT; $s++) {
                    $from = $day->addHours($s * TimeSlotDefinitions::SLOT_HOURS_INTERVAL);
                    $to = $from->addMinutes(TimeSlotDefinitions::SLOT_MINUTES_DURATION);
                    TimeSlot::query()->create([
                        'from' => $from,
                        'to' => $to,
                    ]);
                }
            }
        }
    }
}
