<?php

namespace Database\Seeders;

use App\Models\Room\Room;
use App\Models\RoomTimeSlot\RoomTimeSlot;
use App\Models\TimeSlot\TimeSlot;
use Illuminate\Database\Seeder;

class RoomTimeSlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!RoomTimeSlot::query()->exists())
            foreach (Room::all() as $room) {
                foreach (TimeSlot::all() as $timeSlot) {
                    $room->timeSlots()->attach($timeSlot);
                }
            }
    }
}
