<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomTimeslotUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_timeslot_user', function (Blueprint $table) {
            $table->id();
            $table->foreignId('room_timeslot_id')->constrained('room_timeslot');
            $table->foreignId('user_id')->constrained('users');
            $table->unsignedFloat('discount');
            $table->unsignedFloat('price_after_discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_timeslot_user');
    }
}
