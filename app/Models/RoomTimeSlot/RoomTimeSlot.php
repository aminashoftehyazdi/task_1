<?php

namespace App\Models\RoomTimeSlot;

use App\Definitions\DiscountDefinitions;
use App\Models\RoomTimeSlot\Traits\RoomTimeSlotRelationsTrait;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $room
 * @property mixed $timeslot
 * @property mixed $id
 */
class RoomTimeSlot extends Model
{

    use RoomTimeSlotRelationsTrait;

    protected $table = 'room_timeslot';


    public function booking(User $user): array
    {
        $conditionCheck = $this->conditionCheck($user, $this);

        if (!$conditionCheck['success'])
            return $conditionCheck;

        $priceAfterDiscount = $this->priceAfterDiscount($user);

        $user->roomTimeslots()->attach([$this->id =>
            [
                'discount' => $this->room->price - $priceAfterDiscount,
                'price_after_discount' => $priceAfterDiscount,
            ]
        ]);

        return array(
            'success' => $conditionCheck['success'],
            'data' => $this
        );
    }


    private function conditionCheck(User $user, RoomTimeSlot $roomTimeSlot): array
    {
        $passed = false;
        $data = null;

        if ($user->roomTimeslots->contains($roomTimeSlot->id))
            $data = 'duplicate';
        else if ($roomTimeSlot->room->capacity <= $roomTimeSlot->users()->count())
            $data = 'maximum-reached';
        else
            $passed = true;

        return
            [
                'success' => $passed,
                'data' => $data
            ];
    }


    private function priceAfterDiscount(User $user): int
    {
        $price = $this->room->price;
        $birthDateDiscount = 0;

        if ($user->birth_date)
            if ($user->birth_date->month === $this->timeslot->from->month)
                $birthDateDiscount = $user->birth_date->daysInMonth === $this->timeslot->from->daysInMonth
                    ? DiscountDefinitions::BIRTH_DATE_DISCOUNT
                    : 0;

        return $price -
            ($price * ($birthDateDiscount / 100));
    }

}