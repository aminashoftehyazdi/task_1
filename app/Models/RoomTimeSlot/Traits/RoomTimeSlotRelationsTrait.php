<?php

namespace App\Models\RoomTimeSlot\Traits;

use App\Models\Room\Room;
use App\Models\TimeSlot\TimeSlot;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait RoomTimeSlotRelationsTrait
{
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class,'room_id','id');
    }


    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'room_timeslot_user','room_timeslot_id','user_id');
    }

    public function timeslot(): BelongsTo {
        return $this->belongsTo(TimeSlot::class,'timeslot_id','id');
    }
}