<?php

namespace App\Models\RoomTimeSlotUser;

use Illuminate\Database\Eloquent\Model;

class RoomTimeSlotUser extends Model
{
    protected $table = 'room_timeslot_user';
}