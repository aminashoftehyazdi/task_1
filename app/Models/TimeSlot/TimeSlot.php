<?php

namespace App\Models\TimeSlot;

use App\Models\TimeSlot\Traits\TimeSlotRelationsTrait;
use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    use TimeSlotRelationsTrait;

    protected $table = 'timeslots';

    protected $casts = [
        'from' => 'datetime',
    ];

}