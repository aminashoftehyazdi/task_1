<?php

namespace App\Models\TimeSlot\Traits;

use App\Models\Room\Room;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait TimeSlotRelationsTrait
{

    public function rooms(): BelongsToMany
    {
        return $this->belongsToMany(Room::class,'room_timeslot','timeslot_id','room_id');
    }
}