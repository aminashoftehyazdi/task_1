<?php

namespace App\Models;

use App\Models\User\Traits\UserRelationsTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property mixed $roomTimeslots
 * @property mixed $birth_date
 * @property mixed $password
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, UserRelationsTrait;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birth_date' => 'datetime',
    ];


    /**
     * @throws ValidationException
     */
    public function authenticating($password)
    {
        if (! $this || ! Hash::check($password, $this->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
    }


    public function bookings(): Collection
    {
        return $this->roomTimeslots()
            ->with([
                'room',
                'timeslot',
            ])
            ->get();
    }

}
