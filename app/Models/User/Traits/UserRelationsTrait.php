<?php

namespace App\Models\User\Traits;

use App\Models\RoomTimeSlot\RoomTimeSlot;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait UserRelationsTrait
{
    public function roomTimeslots(): BelongsToMany
    {
        return $this->belongsToMany(RoomTimeSlot::class, 'room_timeslot_user', 'user_id', 'room_timeslot_id')
            ->withPivot([
                'discount',
                'price_after_discount',
                'id',
            ])
            ->withTimestamps();
    }
}