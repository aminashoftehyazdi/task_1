<?php

namespace App\Models\Room;

use App\Models\Room\Traits\RoomRelationsTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $timeSlots
 */
class Room extends Model
{
    use RoomRelationsTrait;

}