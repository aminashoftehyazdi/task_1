<?php

namespace App\Models\Room\Traits;

use App\Models\TimeSlot\TimeSlot;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait RoomRelationsTrait
{
    public function timeSlots(): BelongsToMany
    {
        return $this->belongsToMany(TimeSlot::class,'room_timeslot','room_id','timeslot_id');
    }
}