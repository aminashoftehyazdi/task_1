<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    /**
     * @throws ValidationException
     */
    public function authenticate(AuthRequest $request): string
    {
        /** @var User $user */
        $user = User::query()
            ->where('email', $request->email)
            ->first();

        $user->authenticating($request->password);

        return $user->createToken($request->device_name)->plainTextToken;
    }

}