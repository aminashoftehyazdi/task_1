<?php

namespace App\Http\Controllers\ScapeRoom;


use App\Http\Requests\BookingRequest;
use App\Models\Room\Room;
use App\Models\RoomTimeSlot\RoomTimeSlot;
use App\Models\RoomTimeSlotUser\RoomTimeSlotUser;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

class ScapeRoomController
{
    public function index()
    {
        return Room::all();
    }


    public function show(Room $room): Room
    {
        return $room;
    }


    public function timeslots(Room $room): Collection
    {
        return $room->timeSlots;
    }


    public function bookings(BookingRequest $request): JsonResponse
    {
        /** @var RoomTimeSlot $roomTimeSlot */
        $roomTimeSlot = RoomTimeSlot::query()
            ->find($request->room_timeslot_id);

        $result = $roomTimeSlot->booking($request->user());

        return response()->json($result);
    }


    public function bookingsIndex(): Collection
    {
        return request()->user()
            ->bookings();
    }


    public function bookingsDelete(RoomTimeSlotUser $roomTimeSlotUser)
    {
        if ($roomTimeSlotUser->delete())
            return response()->json([
                'success' => true,
                'data' => $roomTimeSlotUser,
            ]);
    }


}