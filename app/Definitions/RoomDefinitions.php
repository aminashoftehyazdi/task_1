<?php

namespace App\Definitions;

class RoomDefinitions
{

    const ROOMS = [
        [
            'name' => 'prison',
            'capacity' => 11,
            'price' => '10',
        ],
        [
            'name' => 'lake house',
            'capacity' => 12,
            'price' => '20',
        ],
        [
            'name' => 'office',
            'capacity' => 13,
            'price' => '30',
        ],
        [
            'name' => 'lovely',
            'capacity' => 14,
            'price' => '40',
        ],
        [
            'name' => 'gym',
            'capacity' => 15,
            'price' => '50',
        ],
    ];
}