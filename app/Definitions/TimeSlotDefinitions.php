<?php

namespace App\Definitions;

class TimeSlotDefinitions
{

   const DAY_START_AT = 8;
   const CYCLE_DAYS = 7;
   const SLOT_COUNT = 5;
   const SLOT_HOURS_INTERVAL = 2;
   const SLOT_MINUTES_DURATION = 90;
}