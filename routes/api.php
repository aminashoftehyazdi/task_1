<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dev\DevController;
use App\Http\Controllers\ScapeRoom\ScapeRoomController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

if (env('APP_DEBUG', false))
    Route::get('/dev', [DevController::class, 'dev']);

Route::post('/sanctum/token', [AuthController::class, 'authenticate']);

Route::controller(ScapeRoomController::class)
    ->middleware('auth:api')
    ->group(function () {
        Route::prefix('/escape-rooms')->group(function () {
            Route::get('/', 'index');
            Route::get('/{room}', 'show');
            Route::get('/{room}/time-slots', 'timeslots');
        });
        Route::prefix('/bookings')->group(function(){
            Route::post('/', 'bookings');
            Route::get('/', 'bookingsIndex');
            Route::get('/{roomTimeSlotUser}', 'bookingsDelete');
        });
    });

